'genesis' is a well-known and benchmarked free-electron laser (FEL) numerical simulation code, and it is very useful in the design of FEL facilities, e.g. the world's first hard X-ray FEL at SLAC, California, USA.

Dr. Sven Reich @ PSI (http://genesis.web.psi.ch/) is the original author of 'genesis', source files in this git repository is a modified version of 'genesis', for including the transverse gradient undulator simulation.

Tips: new input parameter: 'TGUalpha' is created for defining the gradient of undulator transverse field, in unit of 1/m.

uploaded to github on Jan. 22nd, 2014

src: source code

bin: compiled binary files for linux and windows platform.

transfered to bitbucket on Jan. 24th, 2014.

--- Tong Zhang

email: zhangtong.AT.sinap.ac.cn
